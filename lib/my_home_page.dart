import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'network_services.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File _imageFile;
  dynamic _pickImageError; //Optional

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Image.network(
                      "https://images.pexels.com/photos/46710/pexels-photo-46710.jpeg",
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width),
                ),
                if (_imageFile != null)
                  Expanded(
                    flex: 4,
                    child: Image.file(
                      _imageFile,
                      fit: BoxFit.cover,
                    ),
                  )
                else
                  Expanded(
                    flex: 4,
                    child: Center(
                      child: Text((_pickImageError == null)
                          ? "Select Image"
                          : _pickImageError),
                    ),
                  ),
              ],
            ),
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                margin: EdgeInsets.only(bottom: 36),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FloatingActionButton(
                      child: Icon(Icons.camera_alt),
                      onPressed: () {
                        _pickImage(ImageSource.camera);
                      },
                    ),
                    FloatingActionButton(
                      child: Icon(Icons.file_upload),
                      onPressed: () {
                        NetWorkService().upload(_imageFile);
                      },
                    ),
                    FloatingActionButton(
                      child: Icon(Icons.photo_library),
                      backgroundColor: Colors.red,
                      onPressed: () {
                        _pickImage(ImageSource.gallery);
                      },
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _pickImage(ImageSource source) async {
    try {
      _imageFile = await ImagePicker.pickImage(
        source: source,
      );
      _cropImage();
    } catch (exception) {
      _pickImageError = exception;
      setState(() {});
    }
  }

  Future<Null> _cropImage() async {
    /**
     * Config Android
     * - gradle.properties (Support AndroidX)
     * - build.gradle (project)
     * - AndroidManifest.xml (Add Activity)
     * - gradle-wrapper.properties
     */
    ImageCropper.cropImage(
      sourcePath: _imageFile.path,
      ratioX: 1.0,
      ratioY: 1.2,
      maxWidth: 500,
      maxHeight: 500,
      toolbarTitle: 'Cropper',
      toolbarColor: Colors.blue,
      toolbarWidgetColor: Colors.white,
      statusBarColor: Colors.black,
      //circleShape: true
    ).then((file) {
      if (file != null) {
        _imageFile = file;
      }
    });

    if (_imageFile != null) {
      setState(() {});
    }
  }
}
